require 'spec_helper'

describe Ingredient do

  [:mass, :volume].each do |t|
    let(t) { Measure::STANDARD_UNITS[t].to_s }
  end

  context "Attributes" do
    it { should respond_to(:text) }
    it { should respond_to(:quantity) }
    it { should respond_to(:extra_information) }
    it { should respond_to(:food_item) }
    it { should respond_to(:recipe) }
    it { should respond_to(:section) }
    it { should respond_to(:measure) }
  end

  context "Relationships" do
    it { should belong_to(:recipe) }
    it { should belong_to(:food_item) }
    it { should belong_to(:measure) }
    it { should belong_to(:section) }
  end

  context "Validation" do
    it { should validate_presence_of :recipe_id }
    it { should validate_presence_of :text }
    it { should ensure_length_of(:text).is_at_least(5).is_at_most(256) }
    it { should ensure_length_of(:extra_information).is_at_most(512) }
    it "should validate assigned sections correctly"

    context "Unique Ingredients Per Recipe" do
      before(:each) do
        Factory.create(:ingredient)
      end
      it { should validate_uniqueness_of(:food_item_id).scoped_to(:recipe_id) }
    end

  end

  context "Security" do
    it { should_not allow_mass_assignment_of :measure_id }
    it { should_not allow_mass_assignment_of :food_item_id }
    it { should_not allow_mass_assignment_of :quantity }
  end

  context "Assigning Parsed Attributes" do

    context "Assignment Of Standard Attributes" do

      subject { Factory.create("ingredient_20#{mass}_of_cheese_grated") }

      it "should correctly assign the raw text" do
        subject.text.should eql "20g of cheese, grated"
      end

      it "should correctly assign quantity" do
        subject.quantity.should eql BigDecimal("20")
      end

      it "should correctly assign further description" do
        subject.extra_information.should eql "grated"
      end

    end

    context "Assignment Of Non-standard Measures" do

      subject { Factory.create("ingredient_20oz_of_cheese_grated") }

      it "should correctly assign the raw text" do
        subject.text.should eql "20oz of cheese, grated"
      end

      it "should correctly assign the converted quantity" do
        subject.quantity.round.should eql "20oz".convert_to(mass).scalar.round
      end

      it "should correctly assign further description" do
        subject.extra_information.should eql "grated"
      end

    end

    context "Assignment of Invalid attributes" do

      subject { Factory.create("invalid_ingredient") }

      it "should correctly assign the raw text" do
        subject.text.should eql "A totally, unparsable, ingredient, here"
      end

      it "should correctly assign the converted quantity" do
        subject.quantity.should be_nil
      end

      it "should correctly assign further description" do
        subject.extra_information.should eql "unparsable, ingredient, here"
      end

    end

  end

end
