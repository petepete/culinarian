require 'spec_helper'

describe Section do

  context "Attributes" do
    it { should respond_to(:name) }
    it { should respond_to(:sort) }
  end

  context "Relationships" do
    it { should belong_to(:recipe) }
  end

  context "Validation" do
    it { should ensure_length_of(:name).is_at_least(2).is_at_most(128) }
    it { should validate_numericality_of(:sort) }
  end

end
