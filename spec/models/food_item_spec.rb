require 'spec_helper'

describe FoodItem do
  it { should have_many(:ingredients) }
  it { should have_many(:recipes).through(:ingredients) }
end
