require 'spec_helper'

describe Recipe do

  context "Attributes" do
    it { should respond_to(:title) }
    it { should respond_to(:slug) }
    it { should respond_to(:description) }
    it { should respond_to(:serving_instruction) }
    it { should respond_to(:user) }
    it { should respond_to(:state) }
    it { should respond_to(:cooking_time) }
    it { should respond_to(:inactive_preparation_time) }
    it { should respond_to(:inactive_preparation_time) }
    it { should respond_to(:preparation_time) }
    it { should respond_to(:cooking_time) }
  end

  context "Relationships" do
    it { should have_many(:ingredients) }
    it { should have_many(:food_items).through(:ingredients) }
  end

  context "Validation" do
    it { should validate_presence_of :user_id }
    it { should ensure_length_of(:title).is_at_least(3).is_at_most(128) }

    context "Preparation Time" do
      it { should validate_numericality_of :preparation_time }
      it { should ensure_inclusion_of(:preparation_time).in_range(1..600) }
    end
    context "Cooking Time" do
      it { should validate_numericality_of :cooking_time }
      it { should ensure_inclusion_of(:cooking_time).in_range(1..600) }
    end
    context "Inactive Preparation Time" do
      it { should ensure_inclusion_of(:inactive_preparation_time).in_range(1..2880) }
      it { should allow_value(nil).for(:inactive_preparation_time) }
    end
  end

  context "Scopes" do

    subject { Recipe }

    context "Published" do

      it { should respond_to(:published) }

      context "Results" do
        before(:each) do
          @sandwich = Factory.create(:recipe)
          @sandwich.initial_edit
          @sandwich.publish
          @cake = Factory.create(:recipe)
        end

        it "should return published recipes" do
          Recipe.published.should include(@sandwich)
        end

        it "should not return unpublished recipes" do
          Recipe.published.should_not include(@cake)
        end

      end

    end
  end

  context "Security" do
    it { should_not allow_mass_assignment_of :user_id }
    it { should_not allow_mass_assignment_of :state }
  end

  context "State Machine" do
    before :each do
      @recipe = Factory.create(:recipe)
    end

    it "should have an initial status of 'initialised'" do
      @recipe.state.should eql('initialised')
    end
  end

end
