require 'spec_helper'

describe Step do

  subject { Factory.create(:step) }

  context "Attributes" do
    it { should respond_to(:recipe) }
    it { should respond_to(:section) }
    it { should respond_to(:sort) }
  end

  context "Validation" do
    it { should validate_presence_of(:text) }
    it { should validate_numericality_of(:sort) }
    it "should validate assigned sections correctly"
    it "should not allow duplicate sequence numbers for steps belonging to the same recipe"
  end

  context "Relationships" do
    it { should belong_to(:recipe) }
    it { should belong_to(:section) }
  end

  context "Assigning an sequence number" do
    it "should correctly assign a sequence number"
  end

end
