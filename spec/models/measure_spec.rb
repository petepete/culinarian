require 'spec_helper'

describe Measure do

  context "Attributes" do
    it { should respond_to(:measure_type) }
    it { should respond_to(:is_convertible?) }
  end

  context "Relationships" do
    it { should have_many(:ingredients) }
  end

  context "Validation" do
    it { should ensure_length_of(:name).is_at_least(1).is_at_most(32) }
  end

  context "Security" do
    it { should_not allow_mass_assignment_of :measure_type }
  end

  context "Conversion Awareness" do

    let(:convertible_measure) { Factory.create(:measure, :measure_type => :volume) }
    let(:unconvertible_measure) { Factory.create(:measure, :measure_type => nil) }

    context "Convert To" do
      it "should identify the conversion target given a convertible measure" do
        convertible_measure.convert_to.should eql(Measure::STANDARD_UNITS[:volume])
      end

      it "should raise an error if called for a unconvertible measure" do
        expect { unconvertible_measure.convert_to }.should raise_error(ConversionError, "#{unconvertible_measure.name} is not a convertible measure")
      end
    end

  end

end
