FactoryGirl.define do
  factory :recipe do
    sequence(:title) { |n| "recipe_title_#{n}" }
    sequence(:description) { |n| "recipe_desc_#{n}" }
    cooking_time 20
    preparation_time 20
    user
  end
end
