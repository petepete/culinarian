FactoryGirl.define do
  factory :ingredient do
    recipe
    sequence(:text) { |n| "#{n}g of plain flour, sifted" }
  end

  factory :ingredient_20g_of_cheese_grated, :class => 'ingredient' do
    recipe
    text '20g of cheese, grated'
  end

  factory :ingredient_20oz_of_cheese_grated, :class => 'ingredient' do
    recipe
    text '20oz of cheese, grated'
  end

  factory :invalid_ingredient, :class => 'ingredient' do
    recipe
    text 'A totally, unparsable, ingredient, here'
  end
end
