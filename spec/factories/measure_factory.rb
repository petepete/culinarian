FactoryGirl.define do
  factory :measure do
    sequence(:name) { |n| "measure_name_#{n}" }
  end
end
