FactoryGirl.define do
  factory :food_item do
    sequence(:name) { |n| "food_item_title_#{n}" }
    sequence(:description) { |n| "food_item_desc_#{n}" }
  end
end
