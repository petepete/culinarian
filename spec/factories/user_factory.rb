FactoryGirl.define do
  factory :user do
    sequence(:email) { |n| "user_email_#{n}@something.org" }
    sequence(:username) { |n| "username#{n}" }
    password "12345678test"
    password_confirmation "12345678test"
  end
end
