require 'spec_helper'

describe IngredientParser do

  context "Attributes" do
    it { should respond_to(:food_item) }
    it { should respond_to(:measure) }
    it { should respond_to(:quantity) }
    it { should respond_to(:extra_information) }
    it { should respond_to(:valid?) }
  end

  context "Textual Extractions" do

    ingredients =  YAML.load_file(File.join(Rails.root, "spec", "data", "parsers", "ingredient_parser_data.yml"))

    ingredients[:valid].each do |text, attributes|
      context "Extracting valid data from '%s'" % text do
        ip = IngredientParser.new(text)
        ip.valid? { should be_true }
        attributes.each do |item, result|
          it "should correctly identify '%s' as '%s'" % [ item, result ] do
            ip.send(item.to_s).should eql(result)
          end
        end
      end
    end

    ingredients[:invalid].each do |text|
      context "Extracting invalid data from '%s'" % text do
        ip = IngredientParser.new(text)

        it "should correctly set the ingredient text" do
          ip.ingredient_text  { should eql(text) }
        end

        it "should correctly set specific attributes to be false" do
          ip.quantity  { should be_nil }
          ip.measure   { should be_nil }
          ip.food_item { should be_nil }
        end

        it "should be invalid" do
          ip.valid? { should_not be_false }
        end
      end

    end

  end

end
