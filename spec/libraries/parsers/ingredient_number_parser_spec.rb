require 'spec_helper'

describe NumberParser do

  context "Attributes" do
    it { should respond_to(:raw_text) }
    it { should respond_to(:number) }
  end

  context "Textual Extractions" do
    expected_results =  YAML.load_file(File.join(Rails.root, "spec", "data", "parsers", "number_parser_data.yml"))

    expected_results.each do |input, output|
      context "Extracting valid number from '%s'" % input do
        NumberParser.new(input).tap do |np|
          it "should correctly convert '%s' to '%s'" % [ input, output ] do
            np.number.should == output
          end
          it "should correctly convert '%s' to a BigDecimal" % input do
            np.number.should be_a(BigDecimal)
          end
        end
      end
    end

  end

end
