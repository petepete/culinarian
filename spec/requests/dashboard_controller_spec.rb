require 'spec_helper'

describe DashboardController do

  context "Displaying the home page" do

    before(:each) do
      get root_path
    end

    it "should include the main navigation bar" do
      response.body.should have_xpath("//header/nav")
    end

    it "should include the login section" do
      response.body.should have_xpath("//header/div[@id='login']")
    end

  end

end
