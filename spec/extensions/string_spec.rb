require 'spec_helper'

describe String do

  context "Attributes" do
    it { should respond_to(:pad) }
  end

  context "Padding" do

    subject { "Lorem Ipsum" }

    it "should correctly pad strings" do
      subject.pad.length.should eql(subject.length + 2)
      subject.pad.should eql(" Lorem Ipsum ")
    end

  end

end
