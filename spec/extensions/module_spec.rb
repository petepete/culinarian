require 'spec_helper'

class Foo
  cleansed_string_accessor :my_cleansed_string_accessor
end

describe "CleansedStringAccessor" do

  before(:all) do
    ip = IngredientParser.new
  end

  subject { Foo.new }

  context "Attributes" do
    it { should respond_to(:my_cleansed_string_accessor) }
  end

  context "Cleansing assigned values" do

    it "should strip assigned strings" do
      subject.my_cleansed_string_accessor = "    peanuts   "
      subject.my_cleansed_string_accessor.should eql("peanuts")
    end

    it "should not tamper with already clean strings" do
      subject.my_cleansed_string_accessor = "peanuts"
      subject.my_cleansed_string_accessor.should eql("peanuts")
    end

  end

  context "Assigning supplied arrays" do

    it "should correctly convert and assign a supplied array" do
      array = ["   two", " whole  ", " oranges  "]
      subject.my_cleansed_string_accessor = array
      subject.my_cleansed_string_accessor.should eql(array.map(&:strip).join(" "))
    end

  end

end
