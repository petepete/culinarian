module SectionValidation

  module InstanceMethods

    private

    def validate_section_relationship
      if self.section && self.recipe.id != self.section.recipe.id
        self.errors.add(:section, "Belongs to invalid section")
      end
    end

  end

  def self.included(receiver)
    receiver.send :include, InstanceMethods
  end

end
