namespace :db do
  namespace :test do
    task :prepare => :environment do
      ["db:test:purge", "db:seed"].each do |task|
        Rake::Task[task].invoke
      end
    end
  end
end
