class NumberParser

  attr_accessor :raw_text, :number

  def initialize(text=nil)
    self.raw_text = text
    parse(text) if text and contains_number?(text)
  end

  def parse(text)
    set_attributes(text)
  end

  def to_s
    @number.to_s
  end

  private

  def set_attributes(text)
    if text.match(/\//)
      set_number_from_fraction(text)
    else
      self.number = BigDecimal.new(text)
    end
  end

  def set_number_from_fraction(text)
    self.number = Fractional.to_f( text.match(/^((\d+\s+)?)\d+\/\d+$/).to_s ).to_d
  end

  def contains_number?(text)
    text.match /^#{NUMBER_MATCH}$/
  end
end
