class IngredientParser

  cleansed_string_accessor :food_item, :measure,  :extra_information, :ingredient_text
  cleansed_numeric_string_accessor :quantity, :multiplier

  def initialize(text=nil)
    parse(text) if text
  end

  def parse(text)
    set_attributes(text)
  end

  def valid?
    self.food_item && (self.measure || self.quantity)
  end

  private

  def set_attributes(text)
    set_ingredient_text(text)
    common_ingredient_structures(self.ingredient_text)
  end

  def set_ingredient_text(raw_ingredient_text)
    IngredientTextParser.new(raw_ingredient_text).tap do |itp|
      self.multiplier        = itp.multiplier
      self.extra_information = itp.extra_information
      self.ingredient_text   = itp.ingredient_text
    end
  end

  def common_ingredient_structures(text)
    case
    when text.match(/.*( a ).*( of )/) then
      self.food_item = words_following "of".pad
      self.measure = words_preceding "of".pad

    when text.match(/\d.*of/) then
      self.quantity, self.measure = *quantity_and_measure
      self.food_item = words_following "of".pad

    when text.match(/^a.*of.*/) then
      self.measure = words_preceding "of".pad
      self.food_item = words_following "of".pad

    when text.match(/^(#{NUMBER_MATCH})\w+/) then
      self.quantity, self.measure = *quantity_and_measure
      self.food_item = words_following @measure

    when text.match(/^(#{NUMBER_MATCH})\s+/) then
      @ingredient_text.match(/^(?<quantity>#{NUMBER_MATCH})\s+(?<food_item>.*)/).tap do |md|
        self.quantity = md[:quantity]
        self.food_item = md[:food_item]
      end

    when text.match(/\s+a\s+/) then
      self.food_item = words_following "a".pad
      self.measure = words_preceding @food_item
    end
  end

  def words_following(keyword)
    @ingredient_text.scan(/(?<=#{keyword})(.*)/)
  end

  def words_preceding(keyword)
    @ingredient_text.scan(/.+(?=#{keyword})/)
  end

  def quantity_and_measure
    [ /^(#{NUMBER_MATCH})(.*)\s+of/, /^(#{NUMBER_MATCH})(\s*\w+)/ ].each do |regex|
      matches = @ingredient_text.match(regex)
      return matches.captures if matches
    end
  end

end
