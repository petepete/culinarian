class IngredientTextParser
  attr_accessor :text, :extra_information, :multiplier, :ingredient_text

  def initialize(text)
    self.text = text
    parse
  end

  private

  def parse
    extract_ingredient_text if extract_extra_information and extract_multiplier
  end

  def extract_ingredient_text
    self.ingredient_text = self.text
  end

  def extract_extra_information
    self.text.tap do |t|
      t.match(/,(?<extra_information_text>.*)/) do |m|
        self.extra_information = m[:extra_information_text]
        self.text = t.sub(m.regexp, '')
      end
    end
  end

  def extract_multiplier
    self.text.tap do |t|
      text.match(/^((?<multiplier>#{NUMBER_MATCH})\s*[#{MULTIPLICATION_CHARACTERS}])/) do |m|
        self.multiplier = m[:multiplier]
        self.text = t.sub(m.regexp, '')
      end
    end
  end

end
