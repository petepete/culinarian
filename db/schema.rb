# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20111227164310) do

  create_table "food_items", :force => true do |t|
    t.string   "name",        :limit => 64, :null => false
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ingredients", :force => true do |t|
    t.integer  "food_item_id"
    t.integer  "measure_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "recipe_id"
    t.string   "text",              :limit => 256,                               :null => false
    t.decimal  "quantity",                         :precision => 6, :scale => 2
    t.string   "extra_information", :limit => 512
    t.integer  "section_id"
    t.integer  "sort"
  end

  create_table "measures", :force => true do |t|
    t.string   "name",         :limit => 32
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "measure_type", :limit => 6
  end

  create_table "recipes", :force => true do |t|
    t.integer  "yield_amount"
    t.integer  "yield_target_id"
    t.integer  "genre_id"
    t.text     "description"
    t.text     "serving_instruction"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "title",                     :limit => 128
    t.integer  "user_id"
    t.integer  "inactive_preparation_time"
    t.string   "state",                     :limit => 12,  :null => false
    t.integer  "preparation_time"
    t.integer  "cooking_time"
    t.string   "slug",                      :limit => 128, :null => false
  end

  create_table "sections", :force => true do |t|
    t.string   "name",       :limit => 128, :null => false
    t.integer  "sort"
    t.integer  "recipe_id",                 :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "steps", :force => true do |t|
    t.text     "text"
    t.integer  "section_id"
    t.integer  "recipe_id",  :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "sort"
  end

  create_table "users", :force => true do |t|
    t.string   "email",                                 :default => "", :null => false
    t.string   "encrypted_password",     :limit => 128, :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                         :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "authentication_token"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "username",               :limit => 64,                  :null => false
  end

  add_index "users", ["authentication_token"], :name => "index_users_on_authentication_token", :unique => true
  add_index "users", ["confirmation_token"], :name => "index_users_on_confirmation_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end
