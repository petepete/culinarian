class ChangeIngredientRawTextToText < ActiveRecord::Migration
  def self.up
    rename_column :ingredients, :raw_text, :text
  end

  def self.down
    rename_column :ingredients, :text, :raw_text
  end
end
