class AddSlugToRecipes < ActiveRecord::Migration
  def change
    add_column :recipes, :slug, :string, :limit => 128, :null => false
  end
end
