class AddExtraInformationToIngredient < ActiveRecord::Migration
  def self.up
    add_column :ingredients, :extra_information, :string, :limit => 512
  end

  def self.down
    remove_column :ingredients, :extra_information
  end
end
