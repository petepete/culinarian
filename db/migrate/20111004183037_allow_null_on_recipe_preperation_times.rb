class AllowNullOnRecipePreperationTimes < ActiveRecord::Migration
  def up
    remove_column :recipes, :preparation_time
    remove_column :recipes, :cooking_time
    add_column :recipes, :preparation_time, :integer
    add_column :recipes, :cooking_time, :integer
  end

  def down
    remove_column :recipes, :cooking_time
    remove_column :recipes, :preparation_time
    add_column :recipes, :cooking_time, :null => false
    add_column :recipes, :preparation_time, :null => false
  end
end
