class CreateRecipes < ActiveRecord::Migration
  def self.up
    create_table :recipes do |t|
      t.integer :yield_amount
      t.references :yield_target
      t.integer :perperation_time
      t.references :genre
      t.text :description
      t.text :serving_instruction
      t.timestamps
    end
  end

  def self.down
    drop_table :recipes
  end
end
