class AddTimeFieldsToRecipe < ActiveRecord::Migration
  def self.up
    remove_column :recipes, :perperation_time
    add_column :recipes, :preparation_time, :integer, :null => false
    add_column :recipes, :cooking_time, :integer, :null => false
  end

  def self.down
    remove_column :recipes, :cooking_time
    remove_column :recipes, :preparation_time
    add_column :recipes, :perperation_time, :integer
  end
end
