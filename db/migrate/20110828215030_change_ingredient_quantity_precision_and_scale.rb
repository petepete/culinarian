class ChangeIngredientQuantityPrecisionAndScale < ActiveRecord::Migration
  def self.up

    change_column :ingredients, :quantity, :decimal, { :scale => 2, :precision => 6 }
  end

  def self.down
    change_column :ingredients, :quantity, :decimal, { :scale => 2, :precision => 4 }
  end

end
