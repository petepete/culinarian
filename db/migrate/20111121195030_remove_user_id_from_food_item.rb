class RemoveUserIdFromFoodItem < ActiveRecord::Migration
  def up
    remove_column :food_items, :user_id
  end

  def down
    add_column :food_items, :user_id, :integer
  end
end
