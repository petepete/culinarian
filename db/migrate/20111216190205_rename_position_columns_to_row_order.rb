class RenamePositionColumnsToRowOrder < ActiveRecord::Migration
  def up
    rename_column :sections, :position, :sort
    rename_column :steps, :position, :sort
    rename_column :ingredients, :position, :sort
  end

  def down
    rename_column :ingredients, :sort, :position
    rename_column :steps, :sort, :position
    rename_column :sections, :sort, :position
  end
end
