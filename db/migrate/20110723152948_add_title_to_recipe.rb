class AddTitleToRecipe < ActiveRecord::Migration
  def self.up
    add_column :recipes, :title, :string, :limit => 128
  end

  def self.down
    remove_column :recipes, :title
  end
end
