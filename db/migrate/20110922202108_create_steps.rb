class CreateSteps < ActiveRecord::Migration
  def change
    create_table :steps do |t|
      t.text :text, :null => :false
      t.references :section, :null => true
      t.references :recipe, :null => false
      t.integer :order, :null => false
      t.timestamps
    end
  end
end
