class AddRawTextToIngredient < ActiveRecord::Migration
  def self.up
    add_column :ingredients, :raw_text, :string, :limit => 256, :null => false
  end

  def self.down
    remove_column :ingredients, :raw_text
  end
end
