class CreateFoodItems < ActiveRecord::Migration
  def self.up
    create_table :food_items do |t|
      t.string :name, :limit => 64, :null => false
      t.text :description
      t.timestamps
    end
  end

  def self.down
    drop_table :food_items
  end
end
