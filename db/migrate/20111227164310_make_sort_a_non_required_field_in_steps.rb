class MakeSortANonRequiredFieldInSteps < ActiveRecord::Migration
  def up
    remove_column :steps, :sort
    add_column :steps, :sort, :integer
  end

  def down
    remove_column :steps, :sort
    add_column :steps, :sort, :integer, :allow_null => false
  end
end
