class RenameOrderFieldsToPosition < ActiveRecord::Migration
  def up
    rename_column :sections, :order, :position
    rename_column :steps, :order, :position
    rename_column :ingredients, :order, :position
  end

  def down
    rename_column :sections, :position, :order
    rename_column :steps, :position, :order
    rename_column :ingredients, :position, :order
  end
end
