class ChangeIngredientQuantityToAllowNull < ActiveRecord::Migration
  def self.up
    remove_column :ingredients, :quantity
    add_column :ingredients, :quantity, :decimal, :precision => 4, :scale => 2, :null => true
  end

  def self.down
    remove_column :ingredients, :quantity
    add_column :ingredients, :quantity, :decimal, :precision => 4, :scale => 2, :null => false
  end
end
