class AddOrderToIngredients < ActiveRecord::Migration
  def change
    add_column :ingredients, :order, :integer
  end
end
