class CreateMeasures < ActiveRecord::Migration
  def self.up
    create_table :measures do |t|
      t.string :name, :limit => 32, :null => :false
      t.string :symbol, :limit => 6
      t.timestamps
    end
  end

  def self.down
    drop_table :measures
  end
end
