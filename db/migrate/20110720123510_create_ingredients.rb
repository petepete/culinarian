class CreateIngredients < ActiveRecord::Migration
  def self.up
    create_table :ingredients do |t|
      t.decimal :quantity, :precision => 4, :scale => 2, :null => false
      t.references :food_item
      t.references :unit
      t.timestamps
    end
  end

  def self.down
    drop_table :ingredients
  end
end
