class CreateMeasureTypes < ActiveRecord::Migration
  def self.up
    create_table :measure_types do |t|
      t.string :name, :limit => 32, :null => :false
      t.timestamps
    end
  end

  def self.down
    drop_table :measure_types
  end
end
