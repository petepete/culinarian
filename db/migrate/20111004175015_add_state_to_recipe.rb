class AddStateToRecipe < ActiveRecord::Migration
  def change
    add_column :recipes, :state, :string, :limit => 12, :null => false
  end
end
