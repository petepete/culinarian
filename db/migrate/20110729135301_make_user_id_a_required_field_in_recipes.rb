class MakeUserIdARequiredFieldInRecipes < ActiveRecord::Migration
  def self.up
    change_column :recipes, :user_id, :integer, :null => :false
  end

  def self.down
    change_column :recipes, :user_id, :integer, :null => :true
  end
end
