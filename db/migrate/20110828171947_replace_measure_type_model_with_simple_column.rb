class ReplaceMeasureTypeModelWithSimpleColumn < ActiveRecord::Migration
  def self.up
    drop_table :measure_types
    remove_column :measures, :measure_type_id
    add_column :measures, :measure_type, :string, :limit => 6
  end

  def self.down
    create_table :measure_types do |t|
      t.string :name, :limit => 32, :null => :false
      t.timestamps
    end
    add_column :measures, :measure_type_id, :integer
    remove_column :measures, :measure_type
  end
end
