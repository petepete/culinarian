class AddUserIdToRecipeAndFoodItem < ActiveRecord::Migration

  def self.up
    add_column :food_items, :user_id, :integer
    add_column :recipes, :user_id, :integer
  end

  def self.down
    remove_column :recipes, :user_id
    remove_column :food_items, :user_id
  end

end
