class AddInactivePreparationTimeToRecipe < ActiveRecord::Migration
  def self.up
    add_column :recipes, :inactive_preparation_time, :integer, :null => true
  end

  def self.down
    remove_column :recipes, :inactive_preparation_time
  end
end
