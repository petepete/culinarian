class CreateSections < ActiveRecord::Migration
  def change
    create_table :sections do |t|
      t.string :name, :limit => 128, :null => false
      t.integer :order, :null => true
      t.belongs_to :recipe, :null => false
      t.timestamps
    end
  end
end
