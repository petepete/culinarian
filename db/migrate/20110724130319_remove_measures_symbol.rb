class RemoveMeasuresSymbol < ActiveRecord::Migration

  def self.up
    remove_column :measures, :symbol
  end

  def self.down
    add_column :measures, :symbol, :string, :limit => 6
  end

end
