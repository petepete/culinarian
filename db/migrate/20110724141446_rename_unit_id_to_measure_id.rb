class RenameUnitIdToMeasureId < ActiveRecord::Migration
  def self.up
    rename_column :ingredients, :unit_id, :measure_id
  end

  def self.down
    rename_column :ingredients, :measure_id, :unit_id
  end
end
