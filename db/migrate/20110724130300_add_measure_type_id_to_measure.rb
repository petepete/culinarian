class AddMeasureTypeIdToMeasure < ActiveRecord::Migration
  def self.up
    add_column(:measures, :measure_type_id, :integer)
  end

  def self.down
    remove_column(:measures, :measure_type_id)
  end
end
