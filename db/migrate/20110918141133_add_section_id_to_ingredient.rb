class AddSectionIdToIngredient < ActiveRecord::Migration
  def change
    add_column :ingredients, :section_id, :integer
  end
end
