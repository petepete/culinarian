# These are the common volume and mass related units from the ruby-units gem
COMMON_MEASURES = {
  :mass =>   %w{  kg kilogram kilograms u lbs lb pound pounds # oz ounce 
                  ounces g gram grams gramme grammes },
  :volume => %w{  ml milliliters millilitre millilitres millilitre l L liter liters litre litres gal gallon gallons qt quart 
                  quarts pt pint pints cu cup cups floz fluid-ounce tbs 
                  tablespoon tablespoons tsp teaspoon teaspoons },
  nil =>     %w{  pinch sprinkling half quarter clove knob bar }

}

COMMON_MEASURES.each do |measure_type, measure|
  measure.each do |measure_name|
    Measure.create do |m|
      m.name         = measure_name
      m.measure_type = measure_type
    end
  end
end
