$(document).ready ->

  $.ajaxSetup({
    beforeSend: (xhr) ->
      xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
    cache: false
  })

  ###
    STEPS
  ###

  $('#steps').sortable({
    cursor: 'move'
    refreshPositions: true
    placeholder: 'placeholder'
    opacity: 0.6
    connectWith: ['.section-steps']
    update: (event,ui) ->
      if (!ui.sender && this == ui.item.parent()[0])
        update_step(_id(ui.item), { sort_position: _step_sort_position(ui.item) })
    receive: (event,ui) ->
      if ui.sender
        update_step(
          _id(ui.item)
          $.extend(
            { sort_position: _step_sort_position(ui.item) }
            { section_id: null }
          )
        )
  })

  $('#step_sections').sortable({
    refreshPositions: true
    placeholder: 'placeholder'
    cursor: 'move'
    opacity: 0.6
    update: ->
      console.log('updating step sections')
  })

  $('.section-steps').sortable({
    refreshPositions: true
    placeholder: 'placeholder'
    cursor: 'move'
    opacity: 0.6
    connectWith: ['#steps', '.section-steps']
    update: (event,ui) ->
      if (!ui.sender && this == ui.item.parent()[0])
        update_step(_id(ui.item), { sort_position: _step_sort_position(ui.item) })
    receive: (event,ui) ->
      if ui.sender
        update_step(
          _id(ui.item)
          $.extend(
            { sort_position: _step_sort_position(ui.item) }
            { section_id: _id(this) }
          )
        )
  })

  ###
    INGREDIENTS
  ###

  $('#ingredients').sortable({
    cursor: 'move'
    refreshPositions: true
    placeholder: 'placeholder'
    opacity: 0.6
    connectWith: ['.section-ingredients']
    update: (event,ui) ->
      if (!ui.sender && this == ui.item.parent()[0])
        update_ingredient(_id(ui.item), { sort_position: _ingredient_sort_position(ui.item) })
    receive: (event,ui) ->
      if ui.sender
        update_ingredient(
          _id(ui.item)
          $.extend(
            { sort_position: _ingredient_sort_position(ui.item) }
            { section_id: null }
          )
        )
  })

  $('#ingredient_sections').sortable({
    refreshPositions: true
    placeholder: 'placeholder'
    cursor: 'move'
    opacity: 0.6
    update: ->
      console.log('updating ingredient sections')
  })

  $('.section-ingredients').sortable({
    refreshPositions: true
    placeholder: 'placeholder'
    cursor: 'move'
    opacity: 0.6
    connectWith: ['#ingredients', '.section-ingredients']
    update: (event,ui) ->
      if (!ui.sender && this == ui.item.parent()[0])
        update_ingredient(_id(ui.item), { sort_position: _ingredient_sort_position(ui.item) })
    receive: (event,ui) ->
      if ui.sender
        update_ingredient(
          _id(ui.item)
          $.extend(
            { sort_position: _ingredient_sort_position(ui.item) }
            { section_id: _id(this) }
          )
        )
  })

###
  UTILITY FUNCTIONS
###

update_step = (id, attributes) ->
  $.ajax("steps/#{id}", { type: 'PUT', data: { step: attributes } } )

update_ingredient = (id, attributes) ->
  $.ajax("ingredients/#{id}", { type: 'PUT', data: { ingredient: attributes } } )

_id = (element) ->
  return $(element).attr('id').match(/\d+$/).pop()

_step_sort_position = (step) ->
  return $('#step_section .step').index(step)

_ingredient_sort_position = (ingredient) ->
  return $('#ingredient_section .ingredient').index(ingredient)
