class Manage::IngredientsController < ApplicationController

  def create
    @ingredient = current_user.recipes.find(params[:recipe_id]).ingredients.new(params[:ingredient])
    if @ingredient.save
      redirect_to edit_manage_recipe_path(@ingredient.recipe)
    end
  end

  def update
    # perhaps the `belonging_to` scope would work here?
    @ingredient = current_user.recipes.find(params[:recipe_id]).ingredients.find(params[:id])
    if @ingredient.update_attributes(params[:ingredient])
      render(:nothing => true)
    end
  end

end
