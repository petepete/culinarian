class Manage::RecipesController < ApplicationController

  before_filter :authenticate_user!

  def index
    @recipes = current_user.recipes

    respond_to do |format|
      format.html
      format.xml
    end
  end

  def show
    @recipe = current_user.recipes.find(params[:id])

    respond_to do |format|
      format.html
      format.xml
    end
  end

  def new
    @recipe = Recipe.new
  end

  def create
    @recipe = current_user.recipes.new(params[:recipe])
    if @recipe.save
      redirect_to edit_manage_recipe_path(@recipe)
    else
      render :new
    end
  end

  def edit
    @recipe = current_user.recipes.find(
      params[:id],
      :include => [ :ingredients, :steps, :sections ],
    )

    @recipe.ingredients.build
  end

  def update
    @recipe = current_user.recipes.find(params[:id])
    if @recipe.update_attributes(params[:recipe])
      redirect_to edit_manage_recipe_path(@recipe)
    else
      render :edit
    end
  end

  def destroy
  end

end
