class Manage::StepsController < ApplicationController

  def create
    @step = current_user.recipes.find(params[:recipe_id]).steps.new(params[:step])
    if @step.save
      redirect_to edit_manage_recipe_path(@step.recipe)
    end
  end

  def update
    # perhaps the `belonging_to` scope would work here?
    @step = current_user.recipes.find(params[:recipe_id]).steps.find(params[:id])
    if @step.update_attributes(params[:step])
      render(:nothing => true)
    end
  end

end
