class RecipesController < ApplicationController

  def index
    @recipes = Recipe.published

    respond_to do |format|
      format.html
      format.xml
    end
  end

  def show
    @recipe = Recipe.published.find(params[:id])

    respond_to do |format|
      format.html
      format.xml
    end
  end

end
