class Step < ActiveRecord::Base
  include SectionValidation
  include RankedModel

  belongs_to :recipe
  belongs_to :section

  validates_presence_of :text
  validates_presence_of :recipe
  validates_numericality_of :sort, :allow_nil => true
  validate :validate_section_relationship

  ranks :sort, :with_same => :recipe_id

  scope :unsectioned, where(:section_id => nil)

end
