class Ingredient < ActiveRecord::Base
  include SectionValidation
  include RankedModel

  belongs_to :recipe
  belongs_to :food_item
  belongs_to :section
  belongs_to :measure

  validates_presence_of :recipe_id, :text
  validates_length_of :text, :within => 5..256
  validates_length_of :extra_information, :maximum => 512
  validates_uniqueness_of :food_item_id, :scope => :recipe_id

  before_validation :parse_text

  validate :validate_section_relationship

  attr_protected :food_item_id, :measure_id, :quantity

  ranks :sort, :with_same => :recipe_id

  scope :unsectioned, where(:section_id => nil)

  def measure=(units_text)
    self.measure_id = Measure.find_or_create_by_name(units_text).id
  end

  def food_item=(food_item_text)
    self.food_item_id = FoodItem.find_or_create_by_name(food_item_text).id
  end

  def parsed?
    self.food_item.present? && (self.measure.present? || self.quantity.present?)
  end

  private

  #TODO refactor this method
  def parse_text
    IngredientParser.new(self.text).tap do |ip|

      self.food_item         = ip.food_item if ip.food_item
      self.extra_information = ip.extra_information if ip.extra_information

      #FIXME before assigning, convert fractions to decimals
      if supplied = Measure.find_by_name(ip.measure) and supplied.is_convertible?
        [ip.quantity, ip.measure].join(" ").convert_to(supplied.convert_to).tap do |converted|
          self.measure  = converted.units
          self.quantity = converted.scalar
        end
      else
        self.quantity = ip.quantity
        self.measure  = ip.measure
      end

    end
  end

end
