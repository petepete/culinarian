class Recipe < ActiveRecord::Base

  extend FriendlyId

  friendly_id :title, :use => [ :slugged, :scoped ], :scope => :user

  has_many :food_items, :through => :ingredients
  has_many :sections, :order => "sort asc"
  has_many :steps, :order => "sort asc"
  has_many :ingredients, :order => "sort asc"
  belongs_to :user

  validates_presence_of :user_id
  validates_numericality_of :preparation_time, :allow_nil => true
  validates_numericality_of :cooking_time, :allow_nil => true
  validates_inclusion_of :cooking_time, :in => 1..600, :allow_nil => true
  validates_inclusion_of :inactive_preparation_time, :in => 1..2880, :allow_nil => true
  validates_inclusion_of :preparation_time, :in => 1..600, :allow_nil => true
  validates_length_of :title, :within => 3..128

  attr_protected :user_id, :state

  attr_accessor :preparation_time_units, :cooking_time_units

  accepts_nested_attributes_for :ingredients, :reject_if => :all_blank
  accepts_nested_attributes_for :steps, :reject_if => :all_blank
  accepts_nested_attributes_for :sections, :reject_if => :all_blank

  scope :published, where(:state => :published)

  state_machine :initial => :initialised do

    event :initial_edit do
      transition :initialised => :in_progress
    end

    event :publish do
      transition :in_progress => :published
    end

  end

end
