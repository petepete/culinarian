class Measure < ActiveRecord::Base
  has_many :ingredients

  validates_length_of :name, :within => 1..32

  attr_protected :measure_type

  STANDARD_UNITS = { :mass => "g", :volume => "ml" }

  def convert_to
    raise ConversionError, "#{self.name} is not a convertible measure" unless self.measure_type
    Measure.find_by_name(STANDARD_UNITS[self.measure_type.to_sym]).name
  end

  def is_convertible?
    self.measure_type and self.name != STANDARD_UNITS[self.measure_type.to_sym]
  end

end
