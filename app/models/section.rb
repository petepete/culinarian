class Section < ActiveRecord::Base
  include RankedModel

  belongs_to :recipe
  has_many :ingredients
  has_many :steps

  validates_length_of :name, :within => 2..128
  validates_numericality_of :sort

  ranks :sort, :with_same => :recipe_id
end
