module IngredientsHelper

  def display_ingredient(ingredient)
    render_ingredient_line_partial(ingredient)
  end

  private

  def render_ingredient_line_partial(ingredient)

    ingredient_lines_path = "/shared/ingredients"

    # TODO this can be made nicer
    if ingredient.quantity and ingredient.measure and ingredient.food_item
      render :partial => File.join(ingredient_lines_path, "quantity_measure_and_food_item"), :locals => { :ingredient => ingredient}
    elsif ingredient.quantity and ingredient.food_item
      render :partial => File.join(ingredient_lines_path, "quantity_and_food_item"), :locals => { :ingredient => ingredient}
    elsif ingredient.measure and ingredient.food_item
      render :partial => File.join(ingredient_lines_path, "measure_and_food_item"), :locals => { :ingredient => ingredient}
    else
      render :partial => File.join(ingredient_lines_path, "default"), :locals => { :ingredient => ingredient}
    end
  end

end
