module RecipesHelper

  def options_for_time_select
    { :minutes => 1, :hours => 60, :days => 1440 }
  end

end
