module NavigationHelper

  def navigation_tabs
    capture_haml do
      haml_tag(:ul, :class => "primary") do
        NAVIGATION_CONFIG.each do |tab|
          tab_content(tab)
        end
      end
    end
  end

  private

  def tab_content(tab, opts={})
    opts.merge! :class => tab["label"]

    return if tab["logged_in"] and not current_user

    if tab["controller_path"] == controller.controller_path
      haml_tag :li, tab["label"], opts.merge({ :class => "active" })
    else
      haml_tag :li, link_to(tab["label"], send(tab["path"])), opts
    end
  end

end
