parser_config = YAML.load_file("#{Rails.root}/config/parsers.yml")
NUMBER_MATCH = parser_config[:number_matchers].join("|")
MULTIPLICATION_CHARACTERS = parser_config[:multiplication_characters].join("")
Dir[File.join(Rails.root,"lib", "parsers", "*.rb")].each { |f| require f }
