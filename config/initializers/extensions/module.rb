class Module

  def cleansed_string_accessor(*symbols)
    symbols.each do |symbol|
      class_eval %{

        def #{symbol}
          @#{symbol}
        end

        def #{symbol}=(value)
        case
          when value.is_a?(Array)
            then @#{symbol} = value.flatten.map(&:strip).join(" ")
          when  value.is_a?(String)
            then @#{symbol} = value.strip
          end
          @#{symbol}
        end

      }
    end
  end

  def cleansed_numeric_string_accessor(*symbols)

    symbols.each do |symbol|
      class_eval %{

        def #{symbol}
          @#{symbol}
        end

        def #{symbol}=(value)
        case
          when value.is_a?(Array)
            then cleansed_string = value.flatten.map(&:strip).join(" ")
          when  value.is_a?(String)
            then cleansed_string = value.strip
          end
          @#{symbol} = NumberParser.new(cleansed_string).number
        end

      }
    end

  end

end
