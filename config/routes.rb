Recipes::Application.routes.draw do

  devise_for :users

  root :to => "dashboard#index"

  namespace :manage do
    resources :recipes do
      resources :ingredients
      resources :steps
    end
  end

  resources :recipes, :only => [ :index ]
  scope ":username" do
    resources :recipes, :only => [ :show, :index ]
  end

end
